<script>
    const searchField = document.querySelector('#search-field');

    const tableBody = document.querySelector('tbody');

    var xhr = new XMLHttpRequest();

    let pageReq;
    let row = document.querySelector('.pagination');

    xhr.onreadystatechange = function() {
        if (this.readyState == 4) {
            const response = JSON.parse(this.responseText);
            let noOfRecordsPerPage = 5;
            tableBody.innerHTML = "";

            if (response.length > 5) {
                pageReq = true;
                row.innerText = "Pages For Filtered Records : ";
                let noOfPages = Math.ceil(response.length / noOfRecordsPerPage);
                let buttons = [];
                for (let i = 0; i < noOfPages; i++) {
                    buttons[i] = document.createElement('button');
                    buttons[i].classList.add('btn');
                    buttons[i].style.margin = "5px";
                    buttons[i].innerText = i + 1;
                    buttons[i].addEventListener('click', e => {
                        open();
                        setFormData('page', e.target.innerText);
                        setFormData('limit', noOfRecordsPerPage);
                        setFormData('q', searchField.value);
                        send();
                    });
                    row.appendChild(buttons[i]);
                }
            } else {
                noOfRecordsPerPage = response.length;
            }
            for (let i = 0; i < noOfRecordsPerPage; i++) {
                let entry = response[i];
                tableBody.innerHTML += `<tr>
                <td><img class="circle" src="images/users/${entry.image_name}" alt="" height="78px"></td>
                <td>${entry.first_name} ${entry.last_name}</td>
                <td>${entry.email}</td>
                <td>${entry.birthdate}</td>
                <td>${entry.telephone}</td>
                <td>${entry.address}</td>
                <td><a href="edit-contact.php?id=${entry.id}" class="btn btn-floating green lighten-2"><i class="material-icons">edit</i></a></td>
                <td><a class="btn btn-floating red lighten-2 modal-trigger del-btn" data-id="${entry.id}" href="#deleteModal"><i class="material-icons">delete_forever</i></a>
                </td>
            </tr>`;
            }
            if (pageReq)
                tableBody.appendChild(row);
            else
                row.innerHTML = "";

        }

    }

    const formData = new FormData();

    searchField.addEventListener('keyup', e => {
        const searchedText = e.target.value;
        pageReq = false;
        open();
        setFormData('q', searchedText);
        for (let key of formData.keys()) {
            if (key == 'page') {
                formData.delete('page');
                formData.delete('limit');
            }
        }
        send();
    });

    function open() {
        xhr.open('POST', '../search-contact.php', true);
    }

    function setFormData(parameter, value) {
        formData.append(parameter, value);
    }

    function send() {
        xhr.send(formData);
    }
</script>