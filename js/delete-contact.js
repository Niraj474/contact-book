let deleteBtns = document.querySelectorAll('.del-btn');

let agreeBtn = document.querySelector('.agree-btn');

const table = document.querySelector('.table');

console.log(table);

table.addEventListener('click', e => {
    const btn = e.target.closest('.del-btn');
    if (btn) {
        const id = btn.dataset.id;
        agreeBtn.setAttribute('href', `delete-contact.php?id=${id}`);
    }
})