//datepicker is a plugin which is provided by jQuery
$(function () {
    $('.datepicker').datepicker({
        minDate: new Date(1900, 1, 1),
        maxDate: new Date(),
        yearRange: 25
    });

    $(".file-field").change(function () {
        var file_input = $(this).children().children('input[type=file]');
        console.log(file_input[0].files[0]);
        if (file_input && file_input[0].files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(file_input[0].files[0]);
            reader.onload = imageIsLoaded;
        }
    });

    function imageIsLoaded(e) {
        console.log("x");
        $("#temp_pic").attr('src', e.target.result);
    }

    //validate is a plugin which is provided by jQuery for validating the data.
    $("#add-contact-form").validate({
        rules: {
            first_name: {
                required: true,
                minlength: 2
            },
            last_name: {
                required: true,
                minlength: 2
            },
            telephone: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            birthdate: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            address: {
                required: true,
                minlength: 5
            },
            pic: {
                required: true
            }
        },

        //below code is used to display which error if data fields are left empty i.e. In form if we leave some blank field then error is displayed that this field cannot be empty. 

        errorElement: 'div',

        errorPlacement: function (error, element) {
            var placement = $(element).data('error');
            //data is a method in jquery which is used to get data related attributes bcoz data attributes are special attributes e.g.data-target,data-error and in this we are requiring error i.e.data-error.

            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
});