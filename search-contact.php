<?php

require_once "./includes/functions.php";

if ($_POST) {
    $searchedText = $_POST['q'];
    $searchedText = sanitize_input($searchedText);
    $query = "SELECT * FROM contacts WHERE first_name LIKE '%{$searchedText}%'";
    if (isset($_POST['page'])) {
        $no_of_records_per_page = $_POST['limit'];
        $page = $_POST['page'];
        $start = ($page - 1) * $no_of_records_per_page;
        $query .= " LIMIT {$start},{$no_of_records_per_page}";
        // echo $query;
    }
    $result = db_select($query);
    if ($result) {
        echo json_encode($result);
    } else {
        echo json_encode($result);
    }
}
